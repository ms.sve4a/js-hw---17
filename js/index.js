let number = parseInt(prompt('Enter step'));
while (isNaN(number)) {
    number = parseInt(prompt('Enter step'));
}

function fibonacci(f0 = 0, f1 = 1, number) {
    if ( number < 0 ) {
        return fibonacci(f0, f1,number+2) - fibonacci(f0, f1,number+1);
    }

    if ( number === 0 ) {
        return f0;
    }

    if ( number < 2 ) {
        return f1;
    }

    return fibonacci(f0, f1,number-1) + fibonacci(f0, f1,number-2);

}

alert(fibonacci (f0, f1, number));

 /*function fibloop(f0 = 0, f1 = 1, step) {
    let a = f0, b = f1, i = step;

    while(i > 0) {
        a = [b, b = b + a][0];
        i--;
    }

    return b;
}*/